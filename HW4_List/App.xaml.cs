﻿﻿﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW4_List
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
            //We wrap the MainPage in a Navigation page for push asyn function
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
