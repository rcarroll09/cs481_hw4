﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace HW4_List
{
    public partial class DetailPage : ContentPage
    {
        public ObservableCollection<weekGame> weeklySched { get; set; }
        public DetailPage(TeamInfo info)
        {
            InitializeComponent();
            Logo.Source = info.Img;
            Team.Text = info.City + " " + info.TeamName;
            string[] scheduleSplit = info.Schedule.Split('/');
            weeklySched = new ObservableCollection<weekGame>();
            for(int i=0; i < scheduleSplit.Length; i++)
            {
                string[] oppSplit = scheduleSplit[i].Split('$');
                weeklySched.Add(new weekGame { week = ("Week " + (i + 1).ToString()), opponent = oppSplit[0], score = oppSplit[1] }); 
               
            }
            Console.WriteLine(weeklySched.Count);
            Schedule.ItemsSource = weeklySched;
            
        }
        void Handle_Refreshing(System.Object sender, System.EventArgs e)
        {
            Schedule.IsRefreshing = false;
        }
    }
}
