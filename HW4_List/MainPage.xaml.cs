﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4_List
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<TeamInfo> TeamList { get; set; }
        
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new TeamInfo();
            TeamList = new ObservableCollection<TeamInfo>();
            TeamList.Add(new TeamInfo { Img = "cheifs.png", City = "Kansas City", TeamName = "Cheifs", Record = "12-4", Schedule = "@ Jacksonville Jaguars$W 40-26/@ Oakland Raiders$W 28-10/Vs Baltimore Ravens$W 33-28/@ Detroit Lions$W 34-30/Vs Indianapolis Colts$L 13-19/Vs Houston Texans$L 24-31/@ Denver Broncos$W 30-6/Vs Green Bay Packers$L 24-31/vs Minnesota Vikings$W 26-23/@ Tennessee Titans$L 32-35/@ Los Angeles Chargers$W 24-17/Bye$/Vs Oakland Raiders$W 40-9/@ New England Patriots$W 23-16/Vs Denver Broncos$W 23-3/@ Chicago Bears$W 26-3/Vs Los Angeles Chargers$W 31-21"});
            TeamList.Add(new TeamInfo { Img = "broncos.png", City = "Denver", TeamName = "Broncos", Record = "7-9", Schedule = "@ Oakland Raiders$L 16-24/Vs Chicago Bears$L 14-16/@ Green Bay Packers$L 16-27/Vs Jacksonville Jaguars$L 24-26/@ Los Angeles Chargers$W 20-13/Vs Tennessee Titans$W 16-0/Vs Kansas City Chiefs$L 6-30/@ Indianapolis Colts$L 13-15/Vs Cleveland Browns$ W24-19/Bye$/@Minnesota Vikings$L 23-27/@ Buffalo Bills$L 3-20/Vs Los Angeles Chargers$W 23-20/@ Houston Texans$W 38-24/@ Kansas City Chiefs$L 3-23/Vs Detroit Lions$W 27-17/Vs Oakland Raiders$W 16-15" });
            TeamList.Add(new TeamInfo { Img = "raiders.png", City = "Oakland", TeamName = "Raiders", Record = "7-9", Schedule = "Vs Denver Broncos$W 24-16/Vs Kansas City Chiefs$L 10-28/@ Minnesota Vikings$L 14-34/@ Indianapolis Colts$W 31-24/Vs Chicago Bears$W 24-21/Bye$/@ Green Bay Packers$L 24-42/@ Houston Texans$L 24-27/Vs Detroit Lions$W 31-24/Vs Los Angeles Chargers$W 26-24/Vs Cincinnati Bengals$W 17-10/@ New York Jets$L 3-34/@ Kansas City Chiefs$L 9-40/Vs Tennessee Titans$L 21-42/Vs Jacksonville Jaguars$L 16-0/@ Los Angeles Chargers$W 24-7/@ Denver Broncos$L 15-16" });
            TeamList.Add(new TeamInfo { Img = "NEWCHARGERS.png", City = "Los Angeles", TeamName = "Chargers", Record = "5-11", Schedule = "Vs Indianapolis Colts$W 30-24/@ Detroit Lions$L 10-13/Vs Houston Texans$L 20-27/@ Miami Dolphins$W 30-10/Vs Denver Broncos$L 13-20/Vs Pittsburgh Steelers$L 17-24/@ Tennessee Titans$L 20-23/@ Chicago Bears$W 17-16/Vs Green Bay Packers$W 26-11/@ Oakland Raiders$L 24-26/Vs Kansas City Chiefs$L 17-24/Bye$/@ Denver Broncos$L 20-23/@ Jacksonville Jaguars$W 45-10/Vs Minnesota Vikings$L 10-39/Vs Oakland Raiders$L 17-24/@ Kansas City Chiefs$L 21-31" });
            HW4.ItemsSource = TeamList;
        }

        void Handle_Refreshing(System.Object sender, System.EventArgs e)
        {
            TeamList= new ObservableCollection<TeamInfo>(TeamList.Reverse());
            HW4.ItemsSource = TeamList;
            HW4.IsRefreshing = false;
        }
        async void Handle_Schedule(System.Object sender, System.EventArgs e)
        {
            var x = (MenuItem)sender;
            TeamInfo y = (TeamInfo)x.CommandParameter;
            await Navigation.PushAsync(new DetailPage(y));
        }
    }
}
