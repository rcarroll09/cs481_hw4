﻿using System;
namespace HW4_List
{
    public class TeamInfo
    {
        public string Img { get; set; }
        public string City { get; set; }
        public string TeamName { get; set; }
        public string Record { get; set; }
        public string Schedule { get; set; }        
    }
}
